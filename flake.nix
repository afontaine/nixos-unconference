{
  description = "A NixOS VM to play with for Summit 2024";

  inputs = {

    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-23.11";
    agenix.url = "github:ryantm/agenix";
  };

  outputs = { self, nixpkgs, agenix }:
    let
      # Change this to your repo's directory!
      projectDirectory = "/Users/andrew/Repo/nixos-unconference";
      supportedSystems =
        [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      lib = nixpkgs.lib;
      greeting = ''
        Hello GitLab Summit 2024!
      '';
    in {
      nixosModules.base = { pkgs, ... }: {
        nix = {
          package = pkgs.nixUnstable;
          extraOptions = ''
            experimental-features = nix-command flakes
          '';
          settings.trusted-users = [ "root" "@wheel" ];
          gc = {
            automatic = true;
            options = "--delete-older-than 10d";
          };
          settings.auto-optimise-store = pkgs.stdenv.isLinux;
        };
        system.stateVersion = "23.11";

        # Configure networking
        networking.useDHCP = false;
        networking.interfaces.eth0.useDHCP = true;

        # Create user "test"
        services.getty.autologinUser = "gitlab";
        users.users.gitlab.isNormalUser = true;

        # Enable passwordless ‘sudo’ for the "test" user
        users.users.gitlab.extraGroups = [ "wheel" ];
        security.sudo.wheelNeedsPassword = false;

        environment.systemPackages = with pkgs; [ wget vim nano curl ];

        services.openssh = {
          enable = true;
          hostKeys = [
            {
              bits = 4096;
              path = "/etc/ssh/ssh_host_rsa_key";
              type = "rsa";
            }
            {
              path = "/home/gitlab/keys/ssh_host_ed25519_key";
              type = "ed25519";
            }
          ];
        };
      };
      nixosModules.vm = { ... }: {
        # Make VM output to the terminal instead of a separate window
        virtualisation.vmVariant.virtualisation.graphics = false;
        virtualisation.vmVariant.virtualisation.forwardPorts = [{
          from = "host";
          host.port = 8080;
          guest.port = 80;
        }];
        virtualisation.vmVariant.virtualisation.sharedDirectories.project = {
          source = projectDirectory;
          target = "/home/gitlab/unconference";
        };

      };
      nixosConfigurations = forAllSystems (system:
        let
          systemParts = (builtins.split "-" system);
          vmSystem = "${(lib.head systemParts)}-linux";
        in lib.nixosSystem {
          system = vmSystem;
          modules = [
            self.nixosModules.base
            self.nixosModules.vm
            agenix.nixosModules.default
            ({ pkgs, config, ... }: {
              services.nginx = {
                enable = true;
                virtualHosts."localhost" = {
                  root = self.packages.${vmSystem}.webRoot;
                };
              };
            })
          ] ++ lib.optional ((builtins.match ".+darwin" system) != null) {
            environment.systemPackages =
              [ agenix.packages.${vmSystem}.default ];
            virtualisation.vmVariant.virtualisation.host.pkgs =
              nixpkgs.legacyPackages.${system};

          };
        });
      packages = forAllSystems (system: {
        default = self.nixosConfigurations.${system}.config.system.build.vm;
        webRoot = let pkgs = nixpkgs.legacyPackages.${system};
        in pkgs.writeTextDir "index.html" greeting;
      });
      devShells = forAllSystems (system:
        let pkgs = nixpkgs.legacyPackages.${system};
        in {
          default = pkgs.mkShell {
            buildInputs = [ agenix.packages.${system}.default ];
          };
        });
      checks = forAllSystems (system:
        let pkgs = nixpkgs.legacyPackages.${system};
        in {
          messageCheck = pkgs.stdenvNoCC.mkDerivation {
            name = "message-check";
            dontBuild = true;
            src = ./.;
            doCheck = true;
            nativeBuildInputs = [ pkgs.ripgrep ];
            checkPhase = ''
              rg GitLab ${self.packages.${system}.webRoot}/index.html
            '';
            installPhase = ''
              mkdir "$out"
            '';
          };
        });
    };
}
