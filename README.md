# NixOS Unconference Workshop

This repository will be our starting point for the NixOS workshop. While this
helps to set up a VM, there are some important instructions to follow to get
things running on your machine.

This workshop is pretty unstructured as well. My main goal is to explain flakes
and secrets. For secrets, we will be trying out
[`age-nix`](https://github.com/ryantm/agenix). While I am comfortable with
[`sops-nix`](https://github.com/Mic92/sops-nix), the fundamental ideas are the
same, and I'm sure we'll be able to figure it out.

## Getting Started

1. Clone this repo!
1. Install `nix`!
  * If you are running `NixOS`:
    * You have `nix`!
  * Anyone else:
    * You can either follow the [official guide], or use the [Determinate
      Systems installer](https://github.com/DeterminateSystems/nix-installer).
      As we are going to be enabling flakes, I recommend the latter.
1. Enable flakes!
  * If you are running `NixOS`:
    * Add the following snippet to your `configuration.nix` and rebuild:
   ```nix
        nix = {
          package = pkgs.nixUnstable;
          extraOptions = ''
            experimental-features = nix-command flakes
          '';
        };
   ``` 
  * If you used the offical installer:
    1. Edit `/etc/nix/nix.conf` and add the following line at the bottom:
    ```
    experimental-features = nix-command flakes
    ```
    1. Restart your `nix-daemon`
      * If you are on MacOS
      ```
      sudo launchctl stop org.nixos.nix-daemon
      sudo launchctl start org.nixos.nix-daemon
      ```
      * If you are on Linux:
        * Probably run `sudo systemctl restart nix-daemon`, but you know your
          distro better than I do.
1. Run this VM!
   * Update the `projectDirectory` variable at `flake.nix:13` to this
     repository's directory
   * If you are on linux:
     * Run `nix run` and wait for the prompt.
   * If you are on MacOS, see below.
   
### Running the VM on MacOS

After looking into this a bunch, I am not sure/able to get you a one-command
install and run for MacOS. The issue is that to build VMs, we need a linux
builder. There's one _for_ MacOS that _is_ packaged as a VM, somehow, but it is
neither clear nor obvious what I have to do to _my_ VM definition to make it
work out of the box.

You'll have to follow the instructions [within the `nixpkgs`
manual](https://nixos.org/manual/nixpkgs/stable/#sec-darwin-builder) to set up
the builder. _DO NOT_ follow the flake usage, as it assume you run
[`nix-darwin`](https://github.com/LnL7/nix-darwin), and `nix-darwin` has a built
in module for that (just one `nix.linux-builder.enable = true;` and rebuild away).

Once your linux builder is running, open another terminal window and `cd` to the
repo's directory. The VM should start up with a simple `nix
run` command... I will be holding time at the beginning of the workshop to
assist if it doesn't. I will also be sharing what I am doing in the event that
nothing is working.

### Now what?

Awesome! Once we're in the session, we'll discuss setting up secrets by actually
setting one up and discuss the anatomy of `flake.nix` file, and what it is
doing.

To exit any VM, you'll have to run `sudo poweroff` to shut it down.
